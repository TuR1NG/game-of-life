import React, { useState, useCallback, useRef } from 'react';
import produce from 'immer';

import classes from './App.module.css';

const numRows = 38;
const numCols = 38;

const operations = [
	[0, 1],
	[0, -1],
	[1, 0],
	[1, -1],
	[1, 1],
	[-1, 0],
	[-1, 1],
	[-1, -1],
];

const createEmptyGrid = () => {
	const rows = [];
	for (let i = 0; i < numRows; i++) {
		rows.push(Array.from(Array(numCols), () => 0));
	}
	return rows;
};

const App = () => {
	const [grid, setGrid] = useState(() => createEmptyGrid());
	const [running, setRunning] = useState(false);

	const runningRef = useRef(running);
	runningRef.current = running;

	const runSimulation = useCallback(() => {
		if (!runningRef.current) {
			console.log('called');
			return;
		}

		setGrid((previousGrid) => {
			return produce(previousGrid, (gridCopy) => {
				for (let i = 0; i < numRows; i++) {
					for (let j = 0; j < numCols; j++) {
						let neighbors = 0;
						operations.forEach(([x, y]) => {
							const newI = i + x;
							const newJ = j + y;
							if (newI >= 0 && newI < numRows && newJ >= 0 && newJ < numCols) {
								neighbors += previousGrid[newI][newJ];
							}
						});

						if (neighbors < 2 || neighbors > 3) {
							gridCopy[i][j] = 0;
						} else if (previousGrid[i][j] === 0 && neighbors === 3) {
							gridCopy[i][j] = 1;
						}
					}
				}
			});
		});

		setTimeout(runSimulation, 500);
	}, []);

	return (
		<div>
			<div style={{ textAlign: 'center', margin: '8px 0' }}>
				<button
					className={classes.button}
					onClick={() => {
						setRunning(!running);
						if (!running) {
							runningRef.current = true;
							runSimulation();
						}
					}}
				>
					{running ? 'stop' : 'start'}
				</button>

				<button
					className={classes.button}
					onClick={() => {
						const rows = [];
						for (let i = 0; i < numRows; i++) {
							rows.push(
								Array.from(Array(numCols), () => (Math.random() > 0.8 ? 1 : 0))
							);
						}
						setGrid(rows);
					}}
				>
					Random
				</button>
				<button className={classes.button} onClick={() => setGrid(createEmptyGrid())}>
					Clear
				</button>
			</div>
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<div style={{ display: 'grid', gridTemplateColumns: `repeat(${numCols}, 20px)` }}>
					{grid.map((rows, i) =>
						rows.map((col, j) => (
							<div
								key={`${i}-${j}`}
								onClick={() => {
									const newGrid = produce(grid, (gridCopy) => {
										gridCopy[i][j] = gridCopy[i][j] ? 0 : 1;
									});
									setGrid(newGrid);
								}}
								style={{
									width: 20,
									height: 20,
									backgroundColor: grid[i][j] ? '#0048AE' : null,
									border: '1px solid black',
								}}
							/>
						))
					)}
				</div>
			</div>
		</div>
	);
};

export default App;
